import pygame

from Charakter import Player
from world import world

class Dash(object):
    def __init__(self):
        self.Player = Player()
        self.world = world(1280,720)
        self.clock = pygame.time.Clock()
        pygame.init()
        pygame.font.init()
        self.screen  = pygame.display.set_mode((1280,720))
        pygame.display.set_caption("Dash")
        self.running = True

        self.A = False
        self.D = False
        self.show_fps = True

    def show_live_fps(self):
        self.myfont = pygame.font.SysFont('Arial', 50)
        FPS = self.myfont.render(str(int(self.clock.get_fps()))+" fps", False, (255, 255, 255))
        if self.show_fps == True:
            self.screen.blit(FPS,(0,0))

    def play(self):
        while self.running:
            self.screen.fill((0,0,0))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

                if event.type == pygame.KEYDOWN:
                    #Player Movement
                    if event.key == pygame.K_w:
                        self.W = True
                    if event.key == pygame.K_s:
                        self.S = True
                    if event.key == pygame.K_a:
                        self.A = True
                    if event.key == pygame.K_d:
                        self.D = True
                    if event.key == pygame.K_q:
                        self.Q = True

                if event.type == pygame.KEYUP:
                    if event.key ==pygame.K_w:
                        self.W = False 
                    if event.key ==pygame.K_s:
                        self.S = False
                    if event.key ==pygame.K_d:
                        self.D = False
                    if event.key ==pygame.K_a:
                        self.A = False
                    if event.key == pygame.K_SPACE:
                        self.is_Space = False
                    if event.key == pygame.K_LCTRL:
                        self.controll = False
                    if event.key == pygame.K_q:
                        self.Q = False

            self.world.paint_world(self.screen)
            self.Player.paint_player(self.screen, self.A, self.D)
            self.show_live_fps()
            #print("Sie haben gerade "+str(int(self.clock.get_fps()))+" fps") 
            self.clock.tick(60)
            pygame.display.update()


Dash =Dash()
Dash.play()