import pygame
import itertools


class Player(object):
    def __init__(self):
        #Größe Player Sprites 50x37
        self.timer = 0
        self.pos_x = 100
        self.pos_y = 100
        self.movement_speed = 10
        self.Animations_geschwindigkeit = 20

        self.Idle_00 = pygame.image.load("PlayerSprites/adventurer-idle-00.png")
        self.Idle_01 = pygame.image.load("PlayerSprites/adventurer-idle-01.png")
        self.Idle_02 = pygame.image.load("PlayerSprites/adventurer-idle-02.png")
        

        self.animationcycle_idle = [self.Idle_00,self.Idle_01,self.Idle_02]
        self.IDLE = itertools.cycle(self.animationcycle_idle)
        self.cycle = self.IDLE
        self.actual_image = next(self.cycle)
        self.animation = "idle"
        self.direction = "right"

    def move_player(self,A,D):
        if A == True:
            self.pos_x = self.pos_x - self.movement_speed
        if D == True:
            self.pos_x = self.pos_x + self.movement_speed

    def animate_player(self):
        self.timer = self.timer + 1

        if self.timer == self.Animations_geschwindigkeit:
            self.timer = 0
            self.actual_image = next(self.cycle)

    def paint_player(self,screen, A, D):
        self.move_player(A,D)
        self.animate_player()
        screen.blit(self.actual_image,(self.pos_x,self.pos_y))
