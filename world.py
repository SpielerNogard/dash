import pygame
import random

class world(object):
    def __init__(self, screenwidth, screenheight):
        self.screenwidth = screenwidth
        self.screenheight = screenheight
        self.bg_cloud_A = pygame.image.load("Grassland/bg_cloud_A.png")
        self.bg_cloud_B = pygame.image.load("Grassland/bg_cloud_B.png")
        self.bg_cloud_C = pygame.image.load("Grassland/bg_cloud_C.png")
        self.bg_cloud_D = pygame.image.load("Grassland/bg_cloud_D.png")

        self.bg1 = pygame.image.load("Grassland/bg1.png")
        self.bg2 = pygame.image.load("Grassland/bg2.png")
        self.bg3 = pygame.image.load("Grassland/bg3.png")
        self.bg4 = pygame.image.load("Grassland/bg4.png")

        self.midground_center = pygame.image.load("Grassland/midground_center.png")

        self.midground_left_A = pygame.image.load("Grassland/midground_left_A.png")
        self.midground_left_A_shadowed = pygame.image.load("Grassland/midground_left_A_shadowed.png")

        self.midground_left_B = pygame.image.load("Grassland/midground_left_B.png")
        self.midground_left_B_shadowed = pygame.image.load("Grassland/midground_left_B_shadowed.png")

        self.midground_left_C = pygame.image.load("Grassland/midground_left_C.png")
        self.midground_left_C_shadowed = pygame.image.load("Grassland/midground_left_C_shadowed.png")

        self.midground_right_A = pygame.image.load("Grassland/midground_right_A.png")
        self.midground_right_A_shadowed = pygame.image.load("Grassland/midground_right_A_shadowed.png")

        self.midground_right_B = pygame.image.load("Grassland/midground_right_B.png")
        self.midground_right_B_shadowed = pygame.image.load("Grassland/midground_right_B_shadowed.png")

        self.midground_right_C = pygame.image.load("Grassland/midground_right_C.png")
        self.midground_right_C_shadowed = pygame.image.load("Grassland/midground_right_C_shadowed.png")

        self.terrain_bottom_center = pygame.image.load("Grassland/terrain_bottom_center.png")
        self.terrain_bottom_left = pygame.image.load("Grassland/terrain_bottom_left.png")
        self.terrain_bottom_right = pygame.image.load("Grassland/terrain_bottom_right.png")

        self.terrain_center = pygame.image.load("Grassland/terrain_center.png")
        self.terrain_center_left = pygame.image.load("Grassland/terrain_center_left.png")
        self.terrain_center_right = pygame.image.load("Grassland/terrain_center_right.png")

        self.terrain_corner_inner_bottom_left = pygame.image.load("Grassland/terrain_corner_inner_bottom_left.png")
        self.terrain_corner_inner_bottom_right = pygame.image.load("Grassland/terrain_corner_inner_bottom_right.png")

        self.terrain_platform_center = pygame.image.load("Grassland/terrain_platform_center.png")
        self.terrain_platform_left = pygame.image.load("Grassland/terrain_platform_left.png")
        self.terrain_platform_right = pygame.image.load("Grassland/terrain_platform_right.png")

        self.terrain_slope_ceiling_A = pygame.image.load("Grassland/terrain_slope_ceiling_A.png")
        self.terrain_slope_ceiling_B = pygame.image.load("Grassland/terrain_slope_ceiling_B.png")

        self.terrain_slope_floor_A = pygame.image.load("Grassland/terrain_slope_floor_A.png")
        self.terrain_slope_floor_B = pygame.image.load("Grassland/terrain_slope_floor_B.png")

        self.terrain_top_center = pygame.image.load("Grassland/terrain_top_center.png")
        self.terrain_top_left = pygame.image.load("Grassland/terrain_top_left.png")
        self.terrain_top_right = pygame.image.load("Grassland/terrain_top_right.png")

        self.generate_world()
        
    def generate_world(self):
        Tilegröße = 16

        h = int(self.screenheight / Tilegröße)
        b = int(self.screenwidth / Tilegröße)
        print(str(h))
        print(str(b))
        Dungeon = [[0 for i in range(b)] for j in range(h)]
        #print(Dungeon)
        
        start = (40,0)
        end=(40,79)

        for i in range(h):
            for j in range(b):
                obstacle_prob = random.randint(1, 100)/100
                # placing an obstacle with a probability of 40%
                if obstacle_prob <= 0.4:
                    Dungeon[i][j] = 1
        #print(Dungeon)
        self.Dungeon = Dungeon
    def paint_world(self,screen):
        self.pos_x = 0
        self.pos_y = 0
        for a in self.Dungeon:
            for b in a:
                if b == 0:
                    screen.blit(self.terrain_top_center,(self.pos_x,self.pos_y))
                self.pos_x = self.pos_x + 16
                if self.pos_x >= self.screenwidth:
                    self.pos_x = 0
            self.pos_y = self.pos_y + 16
            if self.pos_y >= self.screenheight:
                self.pos_y = 0
    def create_underground(self,length,x,y):
        underground = []
        Bauteil = []
        Bauteil.append("undergound_start")
        Bauteil.append(x)
        Bauteil.append(y)
        underground.append(Bauteil)
        for i in range(length):
            if i != 0:
                Bauteil = []
                Bauteil.append("underground_center")
                Bauteil.append(x+i*16)
                Bauteil.append(y)
                underground.append(Bauteil)
        Bauteil = []
        Bauteil.append("underground_end")
        Bauteil.append(x+i*16)
        Bauteil.append(y+i*16)
        underground.append(Bauteil)

        print(underground)

    #underground_start = 98 239 102
    #underground_center = 43 134 46
    #underground_end = 16 217 24
    


World = world(1280,720)
World.create_underground(10,1280,720)

        
