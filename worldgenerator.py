import random



class world_generator(object):
    def __init__(self):
        grassland = ["midground_center","midground_left_B","midground_left_B_shadowed","midground_right_B","midground_right_B_shadowed","terrain_bottom_center","terrain_bottom_left","terrain_bottom_right","terrain_center","terrain_center_left","terrain_center_right","terrain_corner_inner_bottom_left","terrain_corner_inner_bottom_right","terrain_corner_inner_top_left","terrain_corner_inner_top_right","terrain_platform_center","terrain_platform_left","terrain_plattform_right","terrain_top_center","terrain_top_left","terrain_top_right"]

        self.alle_welten = [grassland]
        self.generate_rgb()

    def generate_rgb(self):
        alle_welten_rgb = []
        for a in self.alle_welten:
            for b in a:
                name_with_rgb = []
                name_with_rgb.append(b)
                R = random.randint(0, 255)
                G = random.randint(0, 255)
                B = random.randint(0, 255)
                name_with_rgb.append(R)
                name_with_rgb.append(G)
                name_with_rgb.append(B)
                self.write_to_file(name_with_rgb)
                alle_welten_rgb.append(name_with_rgb)
        self.write_to_file_all(alle_welten_rgb)

    def write_to_file(self,input):
        f = open("RGB_Werte.txt", "a")
        f.write(str(input))
        f.close()
    
    def write_to_file_all(self,input):
        f = open("RGB_Werte_full_world.txt", "a")
        f.write(str(input))
        f.close()

    def get_RGB_from_picture(self)
World_Gen = world_generator()